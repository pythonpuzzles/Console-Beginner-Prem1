
def example_a():
    print('\nExample A')
    print('~~~~~~~~~')

    name = input("\nEnter your name: ")

    print('Your name is %s' % name)


# Puzzle A -  Your favourite food
#
# Write a program that asks "What is your favourite food?: "
# Get the user's input
# Use string interpolation to print "mmmm {fav_food} sounds tasty!"
def puzzle_a():
    print('\nPuzzle A')
    print('~~~~~~~~~')


def example_b():
    print('\nExample B')
    print('~~~~~~~~~')

    user_input = input("Do you like dogs?: ")

    print("What ??? " + user_input + " ...really?")


# Puzzle B - Upper Case
#
# Write a program that asks the user "Please enter a word: "
# Convert that input to all upper case
# Using string interpolation, not string concatenation, return "What do you mean? {WORD} !!!"
def puzzle_b():
    print('\nPuzzle B')
    print('~~~~~~~~~')


def example_c():
    print('\nExample C')
    print('~~~~~~~~~~~')

    # Normally, you have to escape control characters like backslash \\ or quotes \" \" or \n new line
    print("What's the \"file path\" again?")

    file_path = "C:\\myfolder\\myfile.txt"
    print("The file path is: " + file_path)

    print("This is a backslash \\")

    # r'' means raw mode. Read the string literally and don't interpret control characters like backslash
    print(r"This is a backslash \\")


# Puzzle C – Control Characters
# Escape all the control characters in the examples below, so that the program runs

def puzzle_c():
    print('\nPuzzle C')
    print('~~~~~~~~~~~')

    # example1 = "Yes, you are "Special""
    # print(example1)

    # example2 = "n Line 1 n Line 2 n Line 3"
    # print(example2)

    # example3 = "C:\Documents\MyFolder\MyFile.txt"
    # print(example3)



if __name__ == '__main__':

    # Run the puzzles

    example_a()
    # puzzle_a()

    example_b()
    # puzzle_b()

    example_c()
    # puzzle_c()
